import React from "react";
import { RhInputFormik } from "@rhythm-ui/react";
import { Form, Formik } from "formik";
const form = ({ setProjectname, projectname, description, setDescription }) => {
  function naming(e) {
    setProjectname(e.target.value);
  }
  function desc(e) {
    setDescription(e.target.value);
  }
  return (
    <div className="h-[15%]  w-full flex flex-col  justify-between">
      <h6 className="font-bold  text-sm">Add new Project</h6>
      <Formik
        initialValues={{
          projectname: "",
          description: "",
        }}
      >
        <Form className="flex flex-col ">
          <>
            <RhInputFormik
              className="w-full"
              name="project name"
              placeholder="project name"
              value={projectname}
              onChange={(e) => naming(e)}
            />
            <RhInputFormik
              className="w-full"
              value={description}
              placeholder="Description"
              type={"textarea"}
              name="description"
              onChange={(e) => desc(e)}
            />
          </>
        </Form>
      </Formik>
    </div>
  );
};

export default form;
