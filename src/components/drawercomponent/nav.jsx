import React, { useState } from "react";
import {
  RhAvatar,
  RhImage,
  RhDivider,
  RhDrawer,
  RhIcon,
  RhButton,
} from "@rhythm-ui/react";
export default function Nav() {
  const LOGO_URL =
    "https://tailwindui.com/img/logos/workflow-mark-indigo-600.svg";
  const [isOpen, setIsOpen] = useState(false);
  function cancel() {
    setIsOpen(false);
  }
  function isDraweropen() {
    setIsOpen(false);
  }
  return (
    <div>
      <>
        <RhDrawer
          isOpen={isOpen}
          onClose={isDraweropen}
          className={"bg-slate-300 h-60  mt-16"}
          position="right"
          size="narrow"
          closeOnOutsideClick={true}
          backdrop={true}
          variant="temporary"
        >
          <span
            className="absolute top-2 right-2 cursor-pointer"
            onClick={cancel}
          >
            <RhIcon icon="heroicons:x-mark" className="text-xl" />
          </span>
          <div className="h-full flex items-end">
            <RhButton className="w-full m-2">logout</RhButton>
          </div>
        </RhDrawer>
      </>
      <div className="h-16 fixed top-0 left-0 right-0 z-10 bg-white mx-6 ">
        <div className="flex h-16 justify-between">
          <div className="flex px-2 ">
            <div className="flex-shrink-0 flex items-center">
              <RhImage
                alt="Image-description"
                aspectRatio="auto"
                height="55px"
                src={LOGO_URL}
                width="55px"
                className="dark:bg-inherit"
              />
            </div>
          </div>
          <div>
            <RhAvatar
              name="Ganit Kumar"
              type="text"
              size="lg"
              className="border-2 border-white dark:border-dark-900 mt-2"
              onClick={() => setIsOpen(!isOpen)}
            />
          </div>
        </div>
        <RhDivider className="w-full" />
      </div>
    </div>
  );
}
