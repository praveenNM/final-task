import React, { useState } from "react";
import { List } from "./list";
import {
  RhInput,
  RhInputGroup,
  RhIcon,
  RhCard,
  RhCardBody,
  RhAvatar,
  RhButton,
} from "@rhythm-ui/react";
const card = ({ userlist, setuserlist }) => {
  const [show, setshow] = useState(false);
  function personList(e) {
    const value = e.target.value;
    setuserlist(
      List.filter((item) => {
        if (item.name.toLowerCase().includes(value.toLowerCase())) {
          return item;
        }
      })
    );
  }
  function personSelandDesel(person) {
    setuserlist((prev) => {
      const updatedList = prev.map((user) => {
        if (person.name === user.name) {
          return {
            ...user,
            selected: !user.selected,
          };
        } else {
          return user;
        }
      });
      return updatedList;
    });
  }

  return (
    <div className="h-5/6  w-full flex flex-col justify-between gap-3  ">
      <div className="">
        <h6 className="text-sm font-bold mb-1">Add Users</h6>
        <RhInputGroup>
          <RhIcon icon="fluent:search-32-filled" rotate={1} />
          <RhInput
            type="text"
            placeholder="Search User"
            className=" text-sm w-full border-0"
            onChange={(e) => personList(e)}
          />
          {show && (
            <RhIcon
              icon="iconoir:cancel"
              className="text-lg text-gray-400"
              onClick={() => setshow(!show)}
            />
          )}
        </RhInputGroup>
      </div>
      <RhCard className="w-full h-full  overflow-y-scroll ">
        <RhCardBody className="text-justify p-0">
          {userlist.length > 0 ? (
            userlist.map((person) => {
              return (
                <RhCard
                  className=" border-2 border-gray-500 w-full mb-1 flex justify-between items-center"
                  key={person.name}
                >
                  <RhCardBody className=" p-0 flex items-center gap-1">
                    <RhAvatar
                      name={person.name}
                      type="text"
                      size="base"
                      className="border-2 border-white dark:border-dark-900"
                    />
                    <div className="flex flex-col ">
                      <h6>{person.name}</h6>
                      <p className="m-0">{person.desc}</p>
                    </div>
                  </RhCardBody>
                  <RhButton
                    onClick={(e) => personSelandDesel(person)}
                    className="bg-white hover:bg-white"
                  >
                    {person.selected ? (
                      <RhIcon
                        icon="icon-park-solid:correct"
                        className="mr-2 text-lg text-black"
                      />
                    ) : (
                      <RhIcon
                        icon="mdi:add-bold"
                        className="mr-2 text-2xl text-black "
                      />
                    )}
                  </RhButton>
                </RhCard>
              );
            })
          ) : (
            <div className="text-2xl ">No matches</div>
          )}
        </RhCardBody>
      </RhCard>
    </div>
  );
};

export default card;
