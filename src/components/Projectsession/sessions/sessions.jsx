import React from "react";
import Session from "./session";
import { RhScrollbar } from "@rhythm-ui/react";
const Sessions = () => {
  return (
    <>
      <p className="text-2xl text-black font-bold m-0 mt-6 pb-1">Sessions</p>
      <RhScrollbar style={{ height: "640px", padding: "5px" }}>
        <Session
          session="Session1"
          info="7 jan 2023,1 PM"
          name="Processing"
          icon="ph:camera-bold"
          img1="https://media.istockphoto.com/id/517188688/photo/mountain-landscape.jpg?s=612x612&w=0&k=20&c=A63koPKaCyIwQWOTFBRWXj_PwCrR4cEoOw2S9Q7yVl8="
          img2="https://www.rd.com/wp-content/uploads/2020/04/GettyImages-1093840488-5-scaled.jpg"
          img3="https://img.freepik.com/free-photo/green-field-tree-blue-skygreat-as-backgroundweb-banner-generative-ai_1258-158251.jpg"
          img4="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSbbR4F7jNdAt-RW57Zrjo6g0ZHwJc8sPki5icLLfgm-cNI_AMfQAiHzCTIJptQEb26Oz0&usqp=CAU"
        />
        <Session
          session="Session2"
          info="7 jan 2023,1 PM"
          name="3D"
          icon="bi:play-btn"
          img1="https://media.istockphoto.com/id/517188688/photo/mountain-landscape.jpg?s=612x612&w=0&k=20&c=A63koPKaCyIwQWOTFBRWXj_PwCrR4cEoOw2S9Q7yVl8="
          img2="https://images.unsplash.com/photo-1610878180933-123728745d22?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8Mnx8Y2FuYWRhJTIwbmF0dXJlfGVufDB8fDB8fHww&w=1000&q=80"
          img3="https://img.freepik.com/free-photo/green-field-tree-blue-skygreat-as-backgroundweb-banner-generative-ai_1258-158251.jpg"
          img4="https://www.rd.com/wp-content/uploads/2020/04/GettyImages-471926619-scaled.jpg?fit=700,467"
        />
        <Session
          session="Session3"
          info="7 jan 2023,1 PM"
          icon="ph:camera-bold"
          img1="https://images.unsplash.com/photo-1610878180933-123728745d22?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8Mnx8Y2FuYWRhJTIwbmF0dXJlfGVufDB8fDB8fHww&w=1000&q=80"
          img2="https://images.unsplash.com/photo-1610878180933-123728745d22?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8Mnx8Y2FuYWRhJTIwbmF0dXJlfGVufDB8fDB8fHww&w=1000&q=80"
          img3="https://thumbs.dreamstime.com/b/environment-earth-day-hands-trees-growing-seedlings-bokeh-green-background-female-hand-holding-tree-nature-field-gra-130247647.jpg"
          img4="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSbbR4F7jNdAt-RW57Zrjo6g0ZHwJc8sPki5icLLfgm-cNI_AMfQAiHzCTIJptQEb26Oz0&usqp=CAU"
        />
        <Session
          session="Session4"
          info="7 jan 2023,1 PM"
          icon="ph:camera-bold"
          img1="https://media.istockphoto.com/id/517188688/photo/mountain-landscape.jpg?s=612x612&w=0&k=20&c=A63koPKaCyIwQWOTFBRWXj_PwCrR4cEoOw2S9Q7yVl8="
          img2="https://www.rd.com/wp-content/uploads/2020/04/GettyImages-1093840488-5-scaled.jpg"
          img3="https://img.freepik.com/free-photo/green-field-tree-blue-skygreat-as-backgroundweb-banner-generative-ai_1258-158251.jpg"
          img4="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSbbR4F7jNdAt-RW57Zrjo6g0ZHwJc8sPki5icLLfgm-cNI_AMfQAiHzCTIJptQEb26Oz0&usqp=CAU"
        />
        <Session
          session="Session5"
          info="7 jan 2023,1 PM"
          icon="ph:camera-bold"
          img1="https://media.istockphoto.com/id/517188688/photo/mountain-landscape.jpg?s=612x612&w=0&k=20&c=A63koPKaCyIwQWOTFBRWXj_PwCrR4cEoOw2S9Q7yVl8="
          img2="https://www.rd.com/wp-content/uploads/2020/04/GettyImages-1093840488-5-scaled.jpg"
          img3="https://img.freepik.com/free-photo/green-field-tree-blue-skygreat-as-backgroundweb-banner-generative-ai_1258-158251.jpg"
          img4="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSbbR4F7jNdAt-RW57Zrjo6g0ZHwJc8sPki5icLLfgm-cNI_AMfQAiHzCTIJptQEb26Oz0&usqp=CAU"
        />
        <Session
          session="Session6"
          info="7 jan 2023,1 PM"
          icon="ph:camera-bold"
          img1="https://media.istockphoto.com/id/517188688/photo/mountain-landscape.jpg?s=612x612&w=0&k=20&c=A63koPKaCyIwQWOTFBRWXj_PwCrR4cEoOw2S9Q7yVl8="
          img2="https://www.rd.com/wp-content/uploads/2020/04/GettyImages-1093840488-5-scaled.jpg"
          img3="https://img.freepik.com/free-photo/green-field-tree-blue-skygreat-as-backgroundweb-banner-generative-ai_1258-158251.jpg"
          img4="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSbbR4F7jNdAt-RW57Zrjo6g0ZHwJc8sPki5icLLfgm-cNI_AMfQAiHzCTIJptQEb26Oz0&usqp=CAU"
        />
        <Session
          session="Session7"
          info="7 jan 2023,1 PM"
          icon="ph:camera-bold"
          img1="https://media.istockphoto.com/id/517188688/photo/mountain-landscape.jpg?s=612x612&w=0&k=20&c=A63koPKaCyIwQWOTFBRWXj_PwCrR4cEoOw2S9Q7yVl8="
          img2="https://www.rd.com/wp-content/uploads/2020/04/GettyImages-1093840488-5-scaled.jpg"
          img3="https://img.freepik.com/free-photo/green-field-tree-blue-skygreat-as-backgroundweb-banner-generative-ai_1258-158251.jpg"
          img4="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSbbR4F7jNdAt-RW57Zrjo6g0ZHwJc8sPki5icLLfgm-cNI_AMfQAiHzCTIJptQEb26Oz0&usqp=CAU"
        />
        <Session
          session="Session8"
          info="7 jan 2023,1 PM"
          icon="ph:camera-bold"
          img1="https://media.istockphoto.com/id/517188688/photo/mountain-landscape.jpg?s=612x612&w=0&k=20&c=A63koPKaCyIwQWOTFBRWXj_PwCrR4cEoOw2S9Q7yVl8="
          img2="https://www.rd.com/wp-content/uploads/2020/04/GettyImages-1093840488-5-scaled.jpg"
          img3="https://img.freepik.com/free-photo/green-field-tree-blue-skygreat-as-backgroundweb-banner-generative-ai_1258-158251.jpg"
          img4="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSbbR4F7jNdAt-RW57Zrjo6g0ZHwJc8sPki5icLLfgm-cNI_AMfQAiHzCTIJptQEb26Oz0&usqp=CAU"
        />
        <Session
          session="Session9"
          info="7 jan 2023,1 PM"
          icon="ph:camera-bold"
          img1="https://media.istockphoto.com/id/517188688/photo/mountain-landscape.jpg?s=612x612&w=0&k=20&c=A63koPKaCyIwQWOTFBRWXj_PwCrR4cEoOw2S9Q7yVl8="
          img2="https://www.rd.com/wp-content/uploads/2020/04/GettyImages-1093840488-5-scaled.jpg"
          img3="https://img.freepik.com/free-photo/green-field-tree-blue-skygreat-as-backgroundweb-banner-generative-ai_1258-158251.jpg"
          img4="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSbbR4F7jNdAt-RW57Zrjo6g0ZHwJc8sPki5icLLfgm-cNI_AMfQAiHzCTIJptQEb26Oz0&usqp=CAU"
        />
      </RhScrollbar>
    </>
  );
};

export default Sessions;
