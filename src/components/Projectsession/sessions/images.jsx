import React from "react";
import { RhGalleryItem, RhImage, RhGallery, RhIcon } from "@rhythm-ui/react";

const images = ({ icon, img1, img2, img3, img4 }) => {
  return (
    <RhGallery
      className="flex gap-4  place-items-center h-full w-full pr-1 "
      onSelection={() => {}}
      position="center"
    >
      <RhIcon icon={icon} className="text-5xl text-black" />
      {[
        {
          src: img1,
          alt: "alt text",
          id: "1",
        },
        { src: img2, alt: "alt text", id: "2" },
        { src: img3, alt: "alt text", id: "3" },
        { src: img4, alt: "alt text", id: "4" },
      ].map((item, index) => (
        <RhGalleryItem
          id={item.id}
          className="p-0 m-0 h-36 flex-1  "
          key={index}
        >
          <RhImage
            className="m-0 p-0 h-full w-full object-cover "
            src={item.src}
          />
        </RhGalleryItem>
      ))}
    </RhGallery>
  );
};

export default images;
