import React from "react";
import PicSelect from "./Pages/pictureSelections";
import PicShow from "./Pages/pictureShowCase";
import NameSelect from "./Pages/nameSelectionDrawer";
import Projectsession from "./Pages/projectsessions";
import {
  BrowserRouter as Router,
  Route,
  Link,
  createBrowserRouter,
  createRoutesFromElements,
  RouterProvider,
  Outlet,
} from "react-router-dom";

export default function App() {
  const router = createBrowserRouter(
    createRoutesFromElements(
      <Route>
        <Route path="/" element={<Root />} />
        <Route path="/pictureselection" element={<PicSelect />} />
        <Route path="/pictureshowcase" element={<PicShow />} />
        <Route path="/nameselection" element={<NameSelect />} />
        <Route path="/projectsession" element={<Projectsession />} />
      </Route>
    )
  );
  return (
    <div>
      <RouterProvider router={router} />
    </div>
  );
}
const Root = () => {
  return (
    <>
      <div className="flex justify-around ">
        <Link to="/"></Link>
        <Link to="/pictureselection">Picture-Selection Page</Link>
        <Link to="/pictureshowcase">Picture-ShowCase Page</Link>
        <Link to="/nameselection">Name-selection Page</Link>
        <Link to="/Projectsession">Project-Session</Link>
      </div>
      <Outlet />
    </>
  );
};
