import React from "react";
import { RhGallery, RhGalleryItem } from "@rhythm-ui/react";
import Nav from "../components/drawercomponent/nav";
import {
  RhImage,
  RhIcon,
  RhButton,
  RhDivider,
  RhBreadCrumbs,
  RhBreadCrumbsItem,
} from "@rhythm-ui/react";
export default function WireFrame() {
  const data = [
    { src: "https://picsum.photos/200", alt: "alt text", id: "1" },
    { src: "https://picsum.photos/201", alt: "alt text", id: "2" },
    { src: "https://picsum.photos/202", alt: "alt text", id: "3" },
    { src: "https://picsum.photos/203", alt: "alt text", id: "4" },
    { src: "https://picsum.photos/204", alt: "alt text", id: "5" },
    { src: "https://picsum.photos/205", alt: "alt text", id: "6" },
    { src: "https://picsum.photos/206", alt: "alt text", id: "7" },
    { src: "https://picsum.photos/207", alt: "alt text", id: "8" },
    { src: "https://picsum.photos/208", alt: "alt text", id: "9" },
    { src: "https://picsum.photos/209", alt: "alt text", id: "10" },
    { src: "https://picsum.photos/210", alt: "alt text", id: "11" },
    { src: "https://picsum.photos/211", alt: "alt text", id: "12" },
    { src: "https://picsum.photos/212", alt: "alt text", id: "13" },
    { src: "https://picsum.photos/213", alt: "alt text", id: "14" },
    { src: "https://picsum.photos/214", alt: "alt text", id: "15" },
    { src: "https://picsum.photos/215", alt: "alt text", id: "16" },
    { src: "https://picsum.photos/216", alt: "alt text", id: "17" },
    { src: "https://picsum.photos/217", alt: "alt text", id: "18" },
    { src: "https://picsum.photos/218", alt: "alt text", id: "19" },
    { src: "https://picsum.photos/219", alt: "alt text", id: "20" },
    { src: "https://picsum.photos/220", alt: "alt text", id: "21" },
    { src: "https://picsum.photos/221", alt: "alt text", id: "22" },
    { src: "https://picsum.photos/222", alt: "alt text", id: "23" },
  ];

  return (
    <div>
      <div className="mx-6 my-2 h-[550px]">
        <div>
          <Nav />
          <div className="mt-10">
            <RhBreadCrumbs
              variant="regular"
              className="text-gray-500 mt-2"
              separator={<RhIcon icon="bi:slash-lg" />}
            >
              <RhBreadCrumbsItem
                icon={<RhIcon className="text-gray-400" />}
                label="Project"
              />
              <RhBreadCrumbsItem
                icon={<RhIcon className="text-gray-400" />}
                label="Project 4"
              />
              <RhBreadCrumbsItem
                icon={<RhIcon className="text-gray-400" />}
                label="Session 2"
              />
            </RhBreadCrumbs>
          </div>
          <div className="flex justify-between">
            <div>
              <h1 className="text-2xl mt-2">Session 2</h1>
              <p className="">
                Updated on 16 May 2023, 10:40 am by Ganit Kumar
              </p>
            </div>
            <div className="flex items-end">
              <RhButton variant="secondary">Convert to 3D</RhButton>
            </div>
          </div>{" "}
          <div>
            <RhButton className="bg-transparent text-black border-b-black border-b-4 hover:bg-transparent">
              Material
            </RhButton>
            <RhButton className="bg-transparent text-black hover:bg-transparent hover:border-b-gray-500 border-b-4">
              Output
            </RhButton>
            <RhDivider className="w-full"></RhDivider>
          </div>
          <div className="flex flex-row items-center justify-between">
            <h4 className="ml-6">{data.length} images</h4>
          </div>
        </div>

        <div className="overflow-y-auto h-[calc(100vh-104px)]">
          <div className="p-8 h-[1000px]">
            <RhGallery
              className="grid grid-cols-1 md:grid-cols-3 lg:grid-cols-5 gap-4"
              onSelection={() => {}}
              position="center"
            >
              {data.map((item, index) => (
                <RhGalleryItem id={item.id}>
                  <RhImage className="m-0 w-36 h-28" src={item.src} />
                </RhGalleryItem>
              ))}
            </RhGallery>
          </div>
        </div>
      </div>
    </div>
  );
}
