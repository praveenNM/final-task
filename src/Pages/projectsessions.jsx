import React from "react";
import Nav from "../components/drawercomponent/nav";
import Project from "../components/Projectsession/project/project";
import Sessions from "../components/Projectsession/sessions/sessions";
const Projectselection = () => {
  return (
    <div className="h-screen w-screen p-4 fixed top-0 flex flex-col justify-between">
      <Nav />
      <div className="h-[90%] overflow-hidden ">
        <Project />
        <Sessions />
      </div>
    </div>
  );
};

export default Projectselection;
