import React, { useState } from "react";
import { RhGallery, RhGalleryItem } from "@rhythm-ui/react";
import Nav from "../components/drawercomponent/nav";
import {
  RhImage,
  RhIcon,
  RhButton,
  RhBreadCrumbs,
  RhBreadCrumbsItem,
  RhCarousel,
  RhCarouselItem,
} from "@rhythm-ui/react";
export default function WireFrame() {
  const data = [
    { src: "https://picsum.photos/200", alt: "alt text", id: "1" },
    { src: "https://picsum.photos/201", alt: "alt text", id: "2" },
    { src: "https://picsum.photos/202", alt: "alt text", id: "3" },
    { src: "https://picsum.photos/203", alt: "alt text", id: "4" },
    { src: "https://picsum.photos/204", alt: "alt text", id: "5" },
    { src: "https://picsum.photos/205", alt: "alt text", id: "6" },
    { src: "https://picsum.photos/206", alt: "alt text", id: "7" },
    { src: "https://picsum.photos/207", alt: "alt text", id: "8" },
    { src: "https://picsum.photos/208", alt: "alt text", id: "9" },
    { src: "https://picsum.photos/209", alt: "alt text", id: "10" },
    { src: "https://picsum.photos/210", alt: "alt text", id: "11" },
    { src: "https://picsum.photos/211", alt: "alt text", id: "12" },
    { src: "https://picsum.photos/212", alt: "alt text", id: "13" },
    { src: "https://picsum.photos/213", alt: "alt text", id: "14" },
    { src: "https://picsum.photos/214", alt: "alt text", id: "15" },
    { src: "https://picsum.photos/215", alt: "alt text", id: "16" },
    { src: "https://picsum.photos/216", alt: "alt text", id: "17" },
    { src: "https://picsum.photos/217", alt: "alt text", id: "18" },
    { src: "https://picsum.photos/218", alt: "alt text", id: "19" },
    { src: "https://picsum.photos/219", alt: "alt text", id: "20" },
    { src: "https://picsum.photos/220", alt: "alt text", id: "21" },
    { src: "https://picsum.photos/221", alt: "alt text", id: "22" },
    { src: "https://picsum.photos/222", alt: "alt text", id: "23" },
  ];
  const [selectedImages, setSelectedImages] = useState([]);
  const [selectAll, setSelectAll] = useState(false);
  const [text, setText] = useState("Select All");

  const handleselectAll = () => {
    setText("Deselect All");

    if (selectAll) {
      setText("Select All");
      setSelectAll(false);
    } else {
      setText("Deselect All");
      setSelectAll(true);
    }
  };
  const handleDeselectAll = () => {
    if (selectAll == null) setSelectAll(false);
    else setSelectAll(null);
  };

  return (
    <div>
      <div className="mx-6 my-2 h-[1020px]">
        <Nav />
        <div className="mt-10">
          <RhBreadCrumbs
            variant="regular"
            className="text-gray-500 mt-2"
            separator={<RhIcon icon="bi:slash-lg" />}
          >
            <RhBreadCrumbsItem
              icon={
                <RhIcon
                  icon="fa6-solid:diagram-project"
                  className="text-gray-400"
                />
              }
              label="Project"
            />
            <RhBreadCrumbsItem
              icon={
                <RhIcon
                  icon="ant-design:project-filled"
                  className="text-gray-400"
                />
              }
              label="Project 4"
            />
            <RhBreadCrumbsItem
              icon={
                <RhIcon
                  icon="heroicons:calendar-days-solid"
                  className="text-gray-400"
                />
              }
              label="Session 2"
            />
            <RhBreadCrumbsItem
              icon={<RhIcon icon="bxs:note" className="text-gray-400" />}
              label="Covert to 3D"
            />
          </RhBreadCrumbs>
        </div>
        <div>
          <h1 className="text-2xl mt-2">Select images</h1>
          <p className="">
            Select minimum of 10 images to make a quality 3D image
          </p>
          <div className="flex flex-row justify-between">
            <h4>{data.length} images</h4>
            {selectAll ? (
              <RhButton
                className="w-26 h-full"
                variant="white"
                size="sm"
                onClick={handleselectAll}
              >
                <RhIcon icon="foundation:x" /> Deselect All
              </RhButton>
            ) : (
              <RhButton
                className="w-26 h-full"
                variant="white"
                size="sm"
                onClick={handleselectAll}
              >
                <RhIcon icon="ic:outline-check" /> Select All
              </RhButton>
            )}
          </div>
          <div className="overflow-y-auto h-[calc(100vh-104px)]">
            <div className="p-8 h-[900px]">
              <RhGallery
                className="grid grid-cols-1 md:grid-cols-3 lg:grid-cols-5 gap-4"
                onSelection={(e) => {
                  setSelectedImages(e);
                }}
                position="center"
                select
              >
                {data.map((item, index) => (
                  <RhGalleryItem id={item.id} isSelected={selectAll}>
                    <RhImage className="m-0 w-36 h-28" src={item.src} />
                  </RhGalleryItem>
                ))}
              </RhGallery>
            </div>
          </div>
        </div>
        <div className="fixed bottom-0 left-0 right-0 bg-gray-300 h- shadow-2xl mx-2 p-2">
          <div className="flex flex-row items-center gap-4">
            <p className="m-0 ">{selectedImages.length} images selected</p>
            <RhButton
              variant="white"
              className="bg-transparent m-0"
              onClick={handleDeselectAll}
            >
              deselect all
            </RhButton>
          </div>
          <div className="flex flex-row items-center gap-4 ">
            <div className="w-[90%]">
              <RhCarousel slidesPerView={10} spaceBetween={15} control={true}>
                {selectedImages.map((item, index) => {
                  return (
                    <RhCarouselItem key={index}>
                      <RhImage className="w-28 h-20" src={data[item].src} />
                    </RhCarouselItem>
                  );
                })}
              </RhCarousel>
            </div>
            <div>
              <RhButton className="w-full h-full bg-black" size="sm">
                Start process
              </RhButton>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
