import React from "react";
import ReactDOM from "react-dom/client";
import { RhThemeProvider, RhToastContainer } from "@rhythm-ui/react";
import App from "./App";
import "./index.css";

ReactDOM.createRoot(document.getElementById("root")).render(
    <React.StrictMode>
        <RhThemeProvider theme="light">
            <RhToastContainer />
            <App />
        </RhThemeProvider>
    </React.StrictMode>
);
